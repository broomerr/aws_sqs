# AWS SQS playground
initial state taked from AWS SDK examples  for Python:
https://github.com/awsdocs/aws-doc-sdk-examples/tree/master/python/example_code/sqs

## Details
1. create_queue.py

Will create queue with hardcoded name: **SQS_QUEUE_NAME** in your default AWS region.

2. receive_message.py

Will try to get 1 message from queue (queue searched by name and by substring). If message found then in thread started task mylongaction(). While it is running in main thread continuesly updated message visibility. When task finished - message deleted from queue.

3. send_message.py

Sends several messages to queue.
