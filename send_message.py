# snippet-comment:[These are tags for the AWS doc team's sample catalog. Do not remove.]
# snippet-sourcedescription:[send_message.py demonstrates how to send a message to an Amazon SQS queue.]
# snippet-service:[sqs]
# snippet-keyword:[Amazon Simple Queue Service (Amazon SQS)]
# snippet-keyword:[Python]
# snippet-keyword:[Code Sample]
# snippet-sourcetype:[snippet]
# snippet-sourcedate:[2019-04-29]
# snippet-sourceauthor:[AWS]

# Copyright 2010-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"). You
# may not use this file except in compliance with the License. A copy of
# the License is located at
#
# http://aws.amazon.com/apache2.0/
#
# or in the "license" file accompanying this file. This file is
# distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
# ANY KIND, either express or implied. See the License for the specific
# language governing permissions and limitations under the License.


import logging
import boto3
from botocore.exceptions import ClientError


def send_sqs_message(sqs_queue_url, msg_body):
    """

    :param sqs_queue_url: String URL of existing SQS queue
    :param msg_body: String message body
    :return: Dictionary containing information about the sent message. If
        error, returns None.
    """

    # Send the SQS message
    sqs_client = boto3.client('sqs')
    try:
        msg = sqs_client.send_message(QueueUrl=sqs_queue_url,
                                      MessageBody=msg_body)
    except ClientError as e:
        logging.error(e)
        return None
    return msg


def search_for_queue(gueue_name_substr):
    sqs = boto3.client('sqs')
    response = sqs.list_queues()
    print(f'response: {response}')
    results = [i for i in response['QueueUrls'] if gueue_name_substr in i]
    print(f'results: {results}')
    if len(results) == 1:
        return(results[0])
    else:
        print(f'Found either multiple queues or none: {results}. Please change substring value for queue.')
        return(None)


def main():
    """Exercise send_sqs_message()"""

    # Assign this value before running the program
    sqs_queue_url = 'SQS_QUEUE_NAME'
    sqs_queue_url = search_for_queue(sqs_queue_url)
    if sqs_queue_url is not None:
        logging.info(f'Found SQS queue: {sqs_queue_url}')
    else:
        logging.info(f'Could not found SQS queue.')
        exit(code=1)

    # Set up logging
    logging.basicConfig(level=logging.DEBUG,
                        format='%(levelname)s: %(asctime)s: %(message)s')

    # Send some SQS messages
    for i in range(1, 6):
        msg_body = f'SQS message #{i}'
        msg = send_sqs_message(sqs_queue_url, msg_body)
        if msg is not None:
            logging.info(f'Sent SQS message ID: {msg["MessageId"]}')


if __name__ == '__main__':
    main()
