# snippet-comment:[These are tags for the AWS doc team's sample catalog. Do not remove.]
# snippet-sourcedescription:[receive_message.py demonstrates how to retrieve messages from an Amazon SQS queue.]
# snippet-service:[sqs]
# snippet-keyword:[Amazon Simple Queue Service (Amazon SQS)]
# snippet-keyword:[Python]
# snippet-keyword:[Code Sample]
# snippet-sourcetype:[snippet]
# snippet-sourcedate:[2019-04-29]
# snippet-sourceauthor:[AWS]

# Copyright 2010-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"). You
# may not use this file except in compliance with the License. A copy of
# the License is located at
#
# http://aws.amazon.com/apache2.0/
#
# or in the "license" file accompanying this file. This file is
# distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
# ANY KIND, either express or implied. See the License for the specific
# language governing permissions and limitations under the License.


import logging
import boto3
from botocore.exceptions import ClientError

# import os
import threading
import time


class RunLongActionThreaded(threading.Thread):

    def __init__(self, function_to_run):
        threading.Thread.__init__(self)
        self.runnable = function_to_run

    def run(self):
        self.runnable()


def mylongaction():
    print(f'Message from inside mylongaction: Starting...')
    time.sleep(60)
    print(f'Message from inside mylongaction: Finished.')



def retrieve_sqs_messages(sqs_queue_url, num_msgs=1, wait_time=0, visibility_time=5):
    """Retrieve messages from an SQS queue

    The retrieved messages are not deleted from the queue.

    :param sqs_queue_url: String URL of existing SQS queue
    :param num_msgs: Number of messages to retrieve (1-10)
    :param wait_time: Number of seconds to wait if no messages in queue
    :param visibility_time: Number of seconds to make retrieved messages
        hidden from subsequent retrieval requests
    :return: List of retrieved messages. If no messages are available, returned
        list is empty. If error, returns None.
    """

    # Validate number of messages to retrieve
    if num_msgs < 1:
        num_msgs = 1
    elif num_msgs > 10:
        num_msgs = 10

    sqs_client = boto3.client('sqs')
    # Retrieve messages from an SQS queue
    try:
        msgs = sqs_client.receive_message(QueueUrl=sqs_queue_url,
                                          MaxNumberOfMessages=num_msgs,
                                          WaitTimeSeconds=wait_time,
                                          VisibilityTimeout=visibility_time)
    except ClientError as e:
        logging.error(e)
        return None

    # Return the list of retrieved messages
    print(f'msgs={msgs}')
    if 'Messages' in msgs.keys():
        return msgs['Messages']
    else:
        return []


def delete_sqs_message(sqs_queue_url, msg_receipt_handle):
    """Delete a message from an SQS queue

    :param sqs_queue_url: String URL of existing SQS queue
    :param msg_receipt_handle: Receipt handle value of retrieved message
    """

    # Delete the message from the SQS queue
    sqs_client = boto3.client('sqs')
    sqs_client.delete_message(QueueUrl=sqs_queue_url,
                              ReceiptHandle=msg_receipt_handle)

def search_for_queue_url(gueue_name_substr):
    sqs_client = boto3.client('sqs')
    response = sqs_client.get_queue_url(QueueName=gueue_name_substr)
    print(f'response: {response}')
    if response is not None:
        results = [response['QueueUrl']]
    else:
        responses = sqs_client.list_queues()
        print(f'responses: {responses}')
        results = [i for i in responses['QueueUrls'] if gueue_name_substr in i]
        print(f'results: {results}')
    if len(results) == 1:
        return(results[0])
    else:
        print(f'Found either multiple queues or none: {results}. Please change substring value for queue.')
        return(None)



def main():
    """Exercise retrieve_sqs_messages()"""

    sqs_client = boto3.client('sqs')
    # Set up logging
    logging.basicConfig(level=logging.INFO,
                        format='%(levelname)s: %(asctime)s: %(message)s')


    # Assign this value before running the program
    num_messages = 1
    sqs_queue_name = 'SQS_QUEUE_NAME'
    sqs_queue_url = search_for_queue_url(sqs_queue_name)
    if sqs_queue_url is not None:
        logging.info(f'Found SQS queue: {sqs_queue_url}')
    else:
        logging.info(f'Could not found SQS queue.')
        exit(code=1)


    # Retrieve SQS messages
    msgs = retrieve_sqs_messages(sqs_queue_url, num_messages)
    if msgs is None:
        print(f'Some error happens!')
    elif len(msgs)==0:
        print(f'No messages found in queue!')
    else:
        for msg in msgs:
            logging.info(f'SQS: Message ID: {msg["MessageId"]}, '
                         f'Contents: {msg["Body"]}')
            thread = RunLongActionThreaded(mylongaction)
            thread.start()
            receipt_handle = msg['ReceiptHandle']
            while thread.isAlive():
                sqs_client.change_message_visibility(QueueUrl=sqs_queue_url,
                                              ReceiptHandle=receipt_handle,
                                              VisibilityTimeout=30)
                print(f'Received and changed visibility timeout of message: {msg["MessageId"]}')
                time.sleep(25) #let wakeup a bit earlier then VisibilityTimeout

            print(f'Message from main: Finished.')


            # Remove the message from the queue
            delete_sqs_message(sqs_queue_url, msg['ReceiptHandle'])
            print(f'Message processed and  removed from quque: {msg["MessageId"]}')


if __name__ == '__main__':
    main()
